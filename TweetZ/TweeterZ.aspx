﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TweeterZ.aspx.cs" Inherits="TweetZ.TweetZ" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Tweeter_z</title>
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootswatch/3.0.0/cosmo/bootstrap.min.css" />
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="index.js"></script>

    <style type="text/css">
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
</head>
<body onload="init();">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Tweeter_z
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav"></ul>
                <form class="navbar-form navbar-right">
                    
                </form>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </div>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <section>
                <div class="row">
                    <div class="col-md-6">
                        <h4><b>Tweeter_z</b>, is the front of an small app that is monitoring the Twitter! some of the statistics that it gathers are available bellow. The problem is the DB, if anyone can help store all that data please contact me.</h4>
                        <h4> The table bellow demonstrates the new words that it captures (top 30) and afterwards the top 30 words ordered by hit count. The words have been limited to 4 characters and above.</h4>

                    </div>
                    <div class="col-md-4" style="float:right;">
                        <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
                        <script type="IN/MemberProfile" data-id="http://www.linkedin.com/pub/zioutas-christos/5a/ab6/608" data-format="inline" data-related="false"></script>
                        <a href="https://twitter.com/drakoumel" class="twitter-follow-button" data-show-count="false" data-size="large" data-dnt="true">Follow @drakoumel</a>
<script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'twitter-wjs');</script>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </section>
            <hr>
            <div class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand"></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a>Stats |</a></li>
                            <li class="active"><a id="scope">Scope: </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <table class="table" id="MainTable">
                <thead>
                    <tr>
                        <th>Hits</th>
                        <th>Word</th>
                        <th>first hit</th>
                        <th>last hit</th>
                        <th>where first</th>
                        <th>where last</th>
                        <th>who first</th>
                        <th>who last</th>
                        <th>lang</th>
                    </tr>
                </thead>
                <tbody id="MainTBody">
                </tbody>
            </table>
        </div>
    </div>
    <div class="container">
        <footer>
            <p>&copy; Christos Zioutas</p>
        </footer>
    </div>
    <div>
        <form runat="server">
            <asp:ScriptManager runat="server" ID="scriptManager">
                <Services>
                    <asp:ServiceReference Path="~/WebService1.asmx" />
                </Services>
                <Scripts>
                    <asp:ScriptReference Path="~/index.js" />
                </Scripts>
            </asp:ScriptManager>
        </form>
    </div>
    <!-- /container -->
</body>
</html>
