﻿function init() {
    webservice1proxy = new TweetZ.WebService1();
    webservice1proxy.set_defaultSucceededCallback(SucceededCallback);
    webservice1proxy.set_defaultFailedCallback(FailedCallback);

    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    //setInterval(function () { GetData(); }, 5000);
    GetData();
}
a = null;

function GetData() {
    webservice1proxy.GetData();

}

function SucceededCallback(result) {
    ProcessData(result);
}

function FailedCallback(error, userContext, methodName) {
    if (error !== null) {
        console.log(error, userContext, methodName);
    }
}

function ProcessData(data) {
    var a = parseJSON(data);
    var tr = null;
    $('#scope').text('Out of ' + a[0]['TC'] + ' unique words captured in tweets.');
    for (var i = 0; i < a.length; i++) {
        if (a[i]['new'] == 1) {
            tr = $('<tr></tr>').append($('<td></td>').append(a[i]['w']))
                          .append($('<td></td>').append(a[i]['c']))
                          .append($('<td></td>').append(a[i]['f_occurance']))
                          .append($('<td></td>').append(a[i]['l_occurance']))
                          .append($('<td></td>').append(a[i]['f_location']))
                          .append($('<td></td>').append(a[i]['l_location']))
                          .append($('<td></td>').append(a[i]['f_user']))
                          .append($('<td></td>').append(a[i]['l_user']))
                          .append($('<td></td>').append(a[i]['lang']));
            tr.css('background-color', 'green');
        }
        else {
            tr = $('<tr></tr>').append($('<td></td>').append(a[i]['w']))
                          .append($('<td></td>').append(a[i]['c']))
                          .append($('<td></td>').append(a[i]['f_occurance']))
                          .append($('<td></td>').append(a[i]['l_occurance']))
                          .append($('<td></td>').append(a[i]['f_location']))
                          .append($('<td></td>').append(a[i]['l_location']))
                          .append($('<td></td>').append(a[i]['f_user']))
                          .append($('<td></td>').append(a[i]['l_user']))
                          .append($('<td></td>').append(a[i]['lang']));
        }
        $('#MainTable tr:last').after(tr);

    }
}

function parseJSON(data) {
    return window.JSON && window.JSON.parse ? window.JSON.parse(data) : (new Function("return " + data))();
}